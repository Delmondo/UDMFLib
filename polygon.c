#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "polygon.h"

polygon_t* new_polygon()
{
	polygon_t* polygon = calloc(1, sizeof(polygon_t));
	polygon->head = NULL;
	polygon->tail = NULL;
	
	return polygon;
}

void delete_polygon(polygon_t* polygon)
{
	vertex_t* vertex = polygon->head;
	
	do {
		vertex_t* next = vertex->next;
		delete_vertex(vertex);
		vertex = next;
	} while (vertex != polygon->head);
	
	return free(polygon);	
}

size_t polygon_size(polygon_t* polygon)
{
	vertex_t* vertex = polygon->head;
	
	int i = 0;
	do {
		vertex = vertex->next;
		++i;
	} while (vertex != polygon->head);
	
	return i;
}

vertex_t* polygon_at(polygon_t* polygon, size_t index)
{
	index = index % polygon_size(polygon);
	
	vertex_t* vertex = polygon->head;
	
	size_t i;
	for (i = 0; i < index; ++i) {
		vertex = vertex->next;
	}
	
	return vertex;
}

vertex_t* polygon_push_back(polygon_t* polygon, float x, float y)
{
	if (!polygon->head)
	{
		polygon->head = new_vertex(x, y);
		polygon->tail = polygon->head;
		
		return polygon->head;
	}
	
	vertex_t* oldtail = polygon->tail;
	vertex_t* newtail = new_vertex(x, y);
	// set the new tail's next node to the head (wrap around)
	newtail->next = polygon->head;
	// set the new tail's previous node to the old tail
	newtail->prev = oldtail;
	// set the old tail's next node to the new tail
	oldtail->next = newtail;
	// set the polygon's tail to the new tail
	polygon->tail = newtail;
	
	return newtail;
}

void polygon_pop_back(polygon_t* polygon)
{
	if (!polygon->head) {
		return;
	}
	
	// the new tail is going to be the one before the current tail
	vertex_t* newtail = polygon->tail->prev;
	// loop the new tail back to the head
	newtail->next = polygon->head;
	// free() the current tail
	delete_vertex(polygon->tail);
	// the new tail is now the polygon's tail
	polygon->tail = newtail;
	
	return;
}

vertex_t* polygon_insert(polygon_t* polygon, size_t index, float x, float y)
{
	assert(index < polygon_size(polygon));
	
	vertex_t* vertex = polygon->head;
	
	size_t i;
	for (i = 0; i <= index; ++i) {
		vertex = vertex->next;
	}
	
	vertex_t* oldnext = vertex->next;
	
	vertex_t* newvertex = new_vertex(x, y);
	newvertex->next = oldnext;
	newvertex->prev = vertex;
	
	return newvertex;
}

void polygon_erase(polygon_t* polygon, size_t index)
{
	index = index % polygon_size(polygon);
	
	vertex_t* vertex = polygon->head;
	
	size_t i;
	for (i = 0; i < index; ++i) {
		vertex = vertex->next;
	}
	
	vertex_t* prev = vertex->prev;
	vertex_t* next = vertex->next;
	
	delete_vertex(vertex);
	
	prev->next = next;
	
	return;
}

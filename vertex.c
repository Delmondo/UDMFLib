#include <stdlib.h>
#include "vertex.h"

vertex_t* new_vertex(float x, float y)
{
	vertex_t* vertex = calloc(1, sizeof(vertex_t));
	vertex->x = x;
	vertex->y = y;
	
	return vertex;
}

void delete_vertex(vertex_t* vertex)
{
	return free(vertex);
}

#ifndef VERTEX_H
#define VERTEX_H

typedef struct vertex_t {
	float x, y;
	struct vertex_t *prev, *next;
} vertex_t;

vertex_t*	new_vertex(float, float);
void		delete_vertex(vertex_t*);

#endif // VERTEX_H

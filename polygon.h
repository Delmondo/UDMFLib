#ifndef POLYGON_H
#define POLYGON_H

#include "vertex.h"

typedef struct polygon_t {
	vertex_t *head, *tail;
} polygon_t;

polygon_t*	new_polygon();
void 		delete_polygon(polygon_t* polygon);

size_t		polygon_size(polygon_t*);

vertex_t*	polygon_push_back(polygon_t*, float x, float y);
void		polygon_pop_back(polygon_t*);

vertex_t*	polygon_insert(polygon_t* polygon, size_t index, float x, float y);
void		polygon_erase(polygon_t* polygon, size_t index);

#endif // POLYGON_H
